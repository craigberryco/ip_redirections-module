<?php

return [
    'geocoding_api_key'      => [
        'name'         => 'Google Geocoding API Key',
        'instructions' => 'Add your Google Maps Geocoding API Key here.',
    ],
    'ip_api_key' => [
        'name'         => 'IP-API API KEY',
        'instructions' => 'Add your IP-API API key here.',
    ],
];
