<?php

return [
    'title'       => 'IP Redirections',
    'name'        => 'IP Redirections Module',
    'description' => 'A module for controlling sitewide redirections based on IP reverse geolocation.'
];
