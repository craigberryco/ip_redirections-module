<?php

return [
    'ip_redirects' => [
        'name'   => 'IP Redirects',
        'option' => [
            'read'   => 'Can read IP Redirects?',
            'write'  => 'Can create/edit IP Redirects?',
            'delete' => 'Can delete IP Redirects?',
        ],
    ],
    'ip_coordinates' => [
        'name'   => 'IP Coordinates',
        'option' => [
            'read'   => 'Can read IP Coordinates?',
            'write'  => 'Can create/edit IP Coordinates?',
            'delete' => 'Can delete IP Coordinates?',
        ],
    ],
    'boundaries' => [
        'name'   => 'Boundaries',
        'option' => [
            'read'   => 'Can read Boundaries?',
            'write'  => 'Can create/edit Boundaries?',
            'delete' => 'Can delete Boundaries?',
        ],
    ],
];
