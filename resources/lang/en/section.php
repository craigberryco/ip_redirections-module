<?php

return [
    'ip_redirects' => [
        'title' => 'IP Redirects',
    ],
    'ip_coordinates' => [
        'title' => 'IP Coordinates',
    ],
    'boundaries' => [
        'title' => 'Boundaries',
    ],
];
