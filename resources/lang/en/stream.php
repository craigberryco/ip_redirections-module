<?php

return [
    'ip_redirects' => [
        'name' => 'IP Redirects',
    ],
    'ip_coordinates' => [
        'name' => 'IP Coordinates',
    ],
    'boundaries' => [
        'name' => 'Boundaries',
    ],
];
