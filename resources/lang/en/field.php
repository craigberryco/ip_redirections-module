<?php

return [
    'name' => [
        'name' => 'Name'
    ],
    'boundaries' => [
        'name' => 'Boundaries'
    ],
    'destination_url' => [
        'name' => 'Destination URL'
    ],
    'enabled' => [
        'name' => 'Enabled?'
    ],
    'exclusion_parameter' => [
        'name' => 'Exclusion Parameter',
        'instructions' => 'This is the request query parameter that will allow the bypassing of this redirect. For example, http://example.com?example'
    ],
    'exclusion_url' => [
        'name' => 'Exclusion URL',
    ],
    'location' => [
        'name' => 'Location'
    ],

    // IP Coordinates
    'ipv4' => [
        'name' => 'IPv4'
    ],
    'latitude' => [
        'name' => 'Latitude'
    ],
    'longitude' => [
        'name' => 'Longitude'
    ],

    // boundaries
    'northeast_latitude' => [
        'name' => 'Northeast Latitude'
    ],
    'northeast_longitude' => [
        'name' => 'Northeast Longitude'
    ],
    'southwest_latitude' => [
        'name' => 'Southwest Latitude'
    ],
    'southwest_longitude' => [
        'name' => 'Southwest Longitude'
    ],
    'internal' => [
        'name' => 'Is internal redirect?',
        'instructions' => 'Will only redirect once per session.'
    ]
];
