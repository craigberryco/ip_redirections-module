<?php

return [
    'ip_redirects' => [
        'read',
        'write',
        'delete',
    ],
    'ip_coordinates' => [
        'read',
        'write',
        'delete',
    ],
    'boundaries' => [
        'read',
        'write',
        'delete',
    ],
];
