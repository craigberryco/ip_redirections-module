<?php

return [
    'geocoding_api_key'  => [
        'type'   => 'anomaly.field_type.text',
        'required' => true,
        'bind'     => 'fryiee.module.ip_redirections::api.geocoding_api_key',
        'config' => [],
    ],
    'ip_api_key'  => [
        'required' => false,
        'type'   => 'anomaly.field_type.text',
        'bind'     => 'fryiee.module.ip_redirections::api.ip_api_key',
        'config' => [],
    ]
];