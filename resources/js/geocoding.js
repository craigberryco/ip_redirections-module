// helper function
function on(elSelector, eventName, selector, fn) {
    var element = document.querySelector(elSelector);

    element.addEventListener(eventName, function(event) {
        var possibleTargets = element.querySelectorAll(selector);
        var target = event.target;

        for (var i = 0, l = possibleTargets.length; i < l; i++) {
            var el = target;
            var p = possibleTargets[i];

            while(el && el !== element) {
                if (el === p) {
                    return fn.call(p, event);
                }

                el = el.parentNode;
            }
        }
    });
}

document.addEventListener("DOMContentLoaded", function() {
    on('body', 'click', '.btn__geocode', function(e) {
        e.preventDefault();
        var geocodingKey = e.target.getAttribute('data-geocode-key');
        var inputName = e.target.getAttribute('data-input');
        var inputIterator = inputName.split('_')[1];
        var locationInput = document.querySelector('[name="'+inputName+'"]');
        var resultsContainer = document.querySelector('[data-container-for-geocoding-results-from="'+inputName+'"]');
        var northeastLatitudeInput = document.querySelector('[name="boundaries_'+inputIterator+'_northeast_latitude"]');
        var northeastLongitudeInput = document.querySelector('[name="boundaries_'+inputIterator+'_northeast_longitude"]');
        var southwestLatitudeInput = document.querySelector('[name="boundaries_'+inputIterator+'_southwest_latitude"]');
        var southwestLongitudeInput = document.querySelector('[name="boundaries_'+inputIterator+'_southwest_longitude"]');

        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address='+locationInput.value+'&key='+geocodingKey);
        xhr.responseType = 'json';
        resultsContainer.innerHTML = '';
        xhr.send();

        xhr.onload = function() {
            if (xhr.status !== 200) {
                resultsContainer.innerHTML = '<div class="alert alert-danger">Unable to complete Geocoding API request: '+xhr.statusText+'</div>';
                console.log(`Geocoding API Error ${xhr.status}: ${xhr.statusText}`);
            } else { // show the result
                var data = xhr.response;

                if (data.status === "OK" && data.results.length > 0 && typeof data.results[0].geometry.bounds !== 'undefined') {
                    resultsContainer.innerHTML = '<div class="alert alert-success">Successfully loaded geometry bounds.</div>';
                    northeastLatitudeInput.value = data.results[0].geometry.bounds.northeast.lat;
                    northeastLongitudeInput.value = data.results[0].geometry.bounds.northeast.lng;
                    southwestLatitudeInput.value = data.results[0].geometry.bounds.southwest.lat;
                    southwestLongitudeInput.value = data.results[0].geometry.bounds.southwest.lng;
                } else {
                    resultsContainer.innerHTML = '<div class="alert alert-warning">No boundary object found.</div>'
                }
            }
        };
        xhr.onerror = function() {
            alert("Request failed");
        };
    });
});