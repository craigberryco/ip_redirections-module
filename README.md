# IP Redirections Module

### Configuration
#### Env variables
- `IP_REDIRECTIONS_OVERRIDE` used to test a specific IP. Will make the middleware detect all requests as originating at that IP. Local testing only.
- `IP_REDIRECTIONS_GEOCODING_API_KEY` REQUIRED. An API Key with Google Geocoding API enabled.
- `IP_REDIRECTIONS_IP_API_KEY` Optional in testing, but required if you want to use IP-API to geolocate IPs on production. Default will use the Free tier, which has a limit of 45 requests per minute and will ban frequent users. This module does not provide rate limiting on the free tier - use an API key for production.
- `IP_REDIRECTIONS_DONT_USE_IP_API` - If you want to exclude using IP-API entirely and rely on manually adding IP Coordinates via the Control Panel or via inserting directly into `ip_redirections_ip_coordinates` table.

