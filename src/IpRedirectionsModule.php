<?php namespace Fryiee\IpRedirectionsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class IpRedirectionsModule
 * @package Fryiee\IpRedirectionsModule
 */
class IpRedirectionsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-map-pin';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'ip_redirects' => [
            'buttons' => [
                'new_ip_redirect',
            ],
        ],
        'ip_coordinates' => [
            'buttons' => [
                'new_ip_coordinate',
            ],
        ]
    ];

}
