<?php namespace Fryiee\IpRedirectionsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryInterface;
use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryRepositoryInterface;
use Fryiee\IpRedirectionsModule\Boundary\BoundaryRepository;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsBoundariesEntryModel;
use Fryiee\IpRedirectionsModule\Boundary\BoundaryModel;
use Fryiee\IpRedirectionsModule\Http\Middleware\GeolocateAgainstIpCoordinates;
use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateInterface;
use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateRepositoryInterface;
use Fryiee\IpRedirectionsModule\IpCoordinate\IpCoordinateRepository;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsIpCoordinatesEntryModel;
use Fryiee\IpRedirectionsModule\IpCoordinate\IpCoordinateModel;
use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectInterface;
use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectRepositoryInterface;
use Fryiee\IpRedirectionsModule\IpRedirect\IpRedirectRepository;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsIpRedirectsEntryModel;
use Fryiee\IpRedirectionsModule\IpRedirect\IpRedirectModel;
use Illuminate\Routing\Router;

class IpRedirectionsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/ip_redirections/ip_coordinates'           => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpCoordinatesController@index',
        'admin/ip_redirections/ip_coordinates/create'    => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpCoordinatesController@create',
        'admin/ip_redirections/ip_coordinates/edit/{id}' => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpCoordinatesController@edit',
        'admin/ip_redirections'           => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpRedirectsController@index',
        'admin/ip_redirections/create'    => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpRedirectsController@create',
        'admin/ip_redirections/edit/{id}' => 'Fryiee\IpRedirectionsModule\Http\Controller\Admin\IpRedirectsController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        GeolocateAgainstIpCoordinates::class
        //Fryiee\IpRedirectionsModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        'web' => [
        ],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [
    ];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Fryiee\IpRedirectionsModule\Event\ExampleEvent::class => [
        //    Fryiee\IpRedirectionsModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Fryiee\IpRedirectionsModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        IpRedirectionsBoundariesEntryModel::class => BoundaryModel::class,
        IpRedirectionsIpCoordinatesEntryModel::class => IpCoordinateModel::class,
        IpRedirectionsIpRedirectsEntryModel::class => IpRedirectModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        BoundaryRepositoryInterface::class => BoundaryRepository::class,
        IpCoordinateRepositoryInterface::class => IpCoordinateRepository::class,
        IpRedirectRepositoryInterface::class => IpRedirectRepository::class,
        BoundaryInterface::class => BoundaryModel::class,
        IpCoordinateInterface::class => IpCoordinateModel::class,
        IpRedirectInterface::class => IpRedirectModel::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
