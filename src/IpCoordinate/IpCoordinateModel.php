<?php namespace Fryiee\IpRedirectionsModule\IpCoordinate;

use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateInterface;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsIpCoordinatesEntryModel;

class IpCoordinateModel extends IpRedirectionsIpCoordinatesEntryModel implements IpCoordinateInterface
{

}
