<?php namespace Fryiee\IpRedirectionsModule\IpCoordinate\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface IpCoordinateRepositoryInterface extends EntryRepositoryInterface
{

}
