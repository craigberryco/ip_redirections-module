<?php namespace Fryiee\IpRedirectionsModule\IpCoordinate\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class IpCoordinateTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'ipv4',
        'latitude',
        'longitude'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'ipv4' => [
            'sort_column' => 'ipv4'
        ],
        'latitude' => [
            'sort_column' => 'latitude'
        ],
        'longitude' => [
            'sort_column' => 'longitude'
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
