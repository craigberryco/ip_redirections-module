<?php namespace Fryiee\IpRedirectionsModule\IpCoordinate;

use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class IpCoordinateRepository extends EntryRepository implements IpCoordinateRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var IpCoordinateModel
     */
    protected $model;

    /**
     * Create a new IpCoordinateRepository instance.
     *
     * @param IpCoordinateModel $model
     */
    public function __construct(IpCoordinateModel $model)
    {
        $this->model = $model;
    }
}
