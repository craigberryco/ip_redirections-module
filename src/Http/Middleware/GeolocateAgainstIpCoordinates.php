<?php namespace Fryiee\IpRedirectionsModule\Http\Middleware;

use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;
use Closure;
use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryInterface;
use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryRepositoryInterface;
use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateInterface;
use Fryiee\IpRedirectionsModule\IpCoordinate\Contract\IpCoordinateRepositoryInterface;
use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectInterface;
use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectRepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class GeolocateAgainstIpCoordinates
 * @package Fryiee\IpRedirectionsModule\Http\Middleware
 */
class GeolocateAgainstIpCoordinates
{
    /**
     * @var IpRedirectRepositoryInterface
     */
    protected $ipRedirects;

    /**
     * @var IpCoordinateRepositoryInterface
     */
    protected $ipCoordinates;

    /**
     * @var BoundaryRepositoryInterface
     */
    protected $boundaries;

    /**
     * GeolocateAgainstIpCoordinates constructor.
     * @param IpRedirectRepositoryInterface $ipRedirects
     * @param IpCoordinateRepositoryInterface $ipCoordinates
     * @param BoundaryRepositoryInterface $boundaries
     */
    public function __construct(
        IpRedirectRepositoryInterface $ipRedirects,
        IpCoordinateRepositoryInterface $ipCoordinates,
        BoundaryRepositoryInterface $boundaries
    ) {
        $this->ipCoordinates = $ipCoordinates;
        $this->ipRedirects = $ipRedirects;
        $this->boundaries = $boundaries;
    }

    /**
     * Helper method in case we inject from another service (see proxies, etc).
     *
     * @param Request $request
     * @return mixed
     */
    protected function getIp($request)
    {
        return env('IP_REDIRECTIONS_OVERRIDE') ?? $request->ip();
    }


    /**
     * Say it loud.
     *
     * @param  Request  $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->is(['admin', 'admin/*', 'form/*']) && $request->is('/')) {
            // if there's an active redirect, use it first
            if (session()->has('active_redirect') && !session()->has('internal_redirect')) {
                /** @var IpRedirectInterface|null $redirect */
                $redirect = $this->ipRedirects->find(session()->get('active_redirect'));
                if ($redirect) {
                    if ($redirect->internal && !session()->has('internal_redirect')) {
                        session()->put('internal_redirect', $redirect->getId());
                    }
                    if (!$request->has($redirect->getAttribute('exclusion_parameter'))) {
                        if ($redirect->isEnabled()) {
                            if (session()->get('exclude_from_redirect') !== $redirect->getId()) {
                                return redirect()->to($redirect->getAttribute('destination_url'));
                            }
                        } else {
                            session()->forget('active_redirect');
                        }
                    } else {
                        session()->put('exclude_from_redirect', $redirect->getId());
                    }
                } else {
                    session()->forget('exclude_from_redirect');
                    session()->forget('active_redirect');
                }
            }

            // if the session hasnt been excluded from redirecting
            if (!session()->has('exclude_from_redirect') && !session()->has('internal_redirect') && $request->is('/')) {
                /** @var IpCoordinateInterface|null $ip */
                $ipCoordinate = $this->ipCoordinates->findBy('ipv4', $this->getIp($request));
                if (!$ipCoordinate && env('IP_REDIRECTIONS_DONT_USE_IP_API') !== true) {
                    try {
                        $apiKey = setting_value('fryiee.module.ip_redirections::ip_api_key');
                        if ($apiKey) {
                            $details = file_get_contents('https://pro.ip-api.com/json/' . $this->getIp($request) . '?key=' . $apiKey);
                        } else {
                            $details = file_get_contents('http://ip-api.com/json/' . $this->getIp($request));
                        }
                        $canDecode = true;
                    } catch (\Exception $e) {
                        $canDecode = false;
                    }

                    if ($canDecode) {
                        $ipCoordinateDetails = json_decode($details, true);

                        if ($ipCoordinateDetails['status'] !== 'fail') {
                            $ipCoordinate = $this->ipCoordinates->create([
                                'ipv4' => $this->getIp($request),
                                'latitude' => $ipCoordinateDetails['lat'],
                                'longitude' => $ipCoordinateDetails['lon']
                            ]);
                        }
                    }
                }

                if ($ipCoordinate && $request->is('/')) {
                    /** @var BoundaryInterface|null $boundary */
                    $boundary = $this->boundaries->findByLatitudeLongitudeWhereEnabled(
                        $ipCoordinate->getAttribute('latitude'),
                        $ipCoordinate->getAttribute('longitude')
                    );

                    if ($boundary) {
                        /** @var IpRedirectInterface|null $redirect */
                        $redirect = $boundary->getIpRedirect();
                        if ($redirect->internal && !session()->has('internal_redirect')) {
                            session()->put('internal_redirect', $redirect->getId());
                        }

                        if ($request->has($redirect->getAttribute('exclusion_parameter'))) {
                            session()->forget('active_redirect');
                            session()->put('exclude_from_redirect', $redirect->getId());
                        } else {
                            session()->put('active_redirect', $redirect->getId());
                            return redirect()->to($redirect->getAttribute('destination_url'));
                        }
                    }
                }
            }
        }

        return $next($request);

    }
}
