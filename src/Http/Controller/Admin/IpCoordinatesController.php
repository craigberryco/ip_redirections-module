<?php namespace Fryiee\IpRedirectionsModule\Http\Controller\Admin;

use Fryiee\IpRedirectionsModule\IpCoordinate\Form\IpCoordinateFormBuilder;
use Fryiee\IpRedirectionsModule\IpCoordinate\Table\IpCoordinateTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class IpCoordinatesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param IpCoordinateTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(IpCoordinateTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param IpCoordinateFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(IpCoordinateFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param IpCoordinateFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(IpCoordinateFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
