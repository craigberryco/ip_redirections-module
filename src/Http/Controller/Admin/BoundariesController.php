<?php namespace Fryiee\IpRedirectionsModule\Http\Controller\Admin;

use Fryiee\IpRedirectionsModule\Boundary\Form\BoundaryFormBuilder;
use Fryiee\IpRedirectionsModule\Boundary\Table\BoundaryTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class BoundariesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param BoundaryTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(BoundaryTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param BoundaryFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(BoundaryFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param BoundaryFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(BoundaryFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
