<?php namespace Fryiee\IpRedirectionsModule\Http\Controller\Admin;

use Fryiee\IpRedirectionsModule\IpRedirect\Form\IpRedirectFormBuilder;
use Fryiee\IpRedirectionsModule\IpRedirect\Table\IpRedirectTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class IpRedirectsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param IpRedirectTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(IpRedirectTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param IpRedirectFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(IpRedirectFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param IpRedirectFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(IpRedirectFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
