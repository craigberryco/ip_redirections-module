<?php namespace Fryiee\IpRedirectionsModule\Boundary\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class BoundaryFormSections
 * @package Fryiee\IpRedirectionsModule\Boundary\Form
 */
class BoundaryFormSections
{
    /**
     * @param BoundaryFormBuilder $builder
     */
    public function handle(BoundaryFormBuilder $builder)
    {
        $sections = [

            'location' => [
                'rows' => [
                    [
                        'columns' => [
                            [
                                'classes' => 'col-sm-12 col__boundary--location',
                                'fields' => [
                                    'location'
                                ]
                            ],
                            [
                                'classes' => 'col-sm-12 col__boundary--coordinates',
                                'fields' => [
                                    'southwest_latitude',
                                    'southwest_longitude',
                                    'northeast_latitude',
                                    'northeast_longitude',
                                ]
                            ]
                        ]
                    ]
                ]
            ],
        ];
        $sections = $builder->prefixSectionFields($builder->getOption('prefix'), $sections);

        $builder->setSections($sections);
    }
}
