<?php namespace Fryiee\IpRedirectionsModule\Boundary;

use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryInterface;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsBoundariesEntryModel;
use Fryiee\IpRedirectionsModule\Boundary\Form\BoundaryFormBuilder;
use Fryiee\IpRedirectionsModule\IpRedirect\IpRedirectModel;

class BoundaryModel extends IpRedirectionsBoundariesEntryModel implements BoundaryInterface
{
    /**
     * @return \Illuminate\Foundation\Application|mixed
     */
    public function newRepeaterFieldTypeFormBuilder()
    {
        return app(BoundaryFormBuilder::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ipRedirects()
    {
        return $this->belongsToMany(
            IpRedirectModel::class,
            'ip_redirections_ip_redirects_boundaries',
            'related_id',
            'entry_id'
        );
    }

    /**
     * @return mixed
     */
    public function getIpRedirect()
    {
        return $this->ipRedirects()->first();
    }
}
