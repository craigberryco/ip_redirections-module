<?php namespace Fryiee\IpRedirectionsModule\Boundary\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BoundaryRepositoryInterface extends EntryRepositoryInterface
{
    public function findByLatitudeLongitudeWhereEnabled($latitude, $longitude);
}
