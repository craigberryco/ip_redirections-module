<?php namespace Fryiee\IpRedirectionsModule\Boundary\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface BoundaryInterface
 * @package Fryiee\IpRedirectionsModule\Boundary\Contract
 */
interface BoundaryInterface extends EntryInterface
{
    public function getIpRedirect();
}
