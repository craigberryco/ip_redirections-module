<?php namespace Fryiee\IpRedirectionsModule\Boundary;

use Fryiee\IpRedirectionsModule\Boundary\Contract\BoundaryRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class BoundaryRepository extends EntryRepository implements BoundaryRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BoundaryModel
     */
    protected $model;

    /**
     * Create a new BoundaryRepository instance.
     *
     * @param BoundaryModel $model
     */
    public function __construct(BoundaryModel $model)
    {
        $this->model = $model;
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return mixed
     */
    public function findByLatitudeLongitudeWhereEnabled($latitude, $longitude)
    {
        return $this->getModel()->where('northeast_latitude', '>=', $latitude)
            ->where('southwest_latitude', '<=', $latitude)
            ->where('northeast_longitude', '>=', $longitude)
            ->where('southwest_longitude', '<=', $longitude)
            ->whereHas('ipRedirects', function ($query) {
                $query->where('enabled', true);
            })->first();
    }
}
