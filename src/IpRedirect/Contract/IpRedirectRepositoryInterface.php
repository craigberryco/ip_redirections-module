<?php namespace Fryiee\IpRedirectionsModule\IpRedirect\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface IpRedirectRepositoryInterface extends EntryRepositoryInterface
{

}
