<?php namespace Fryiee\IpRedirectionsModule\IpRedirect\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface IpRedirectInterface extends EntryInterface
{
    public function isEnabled();
}
