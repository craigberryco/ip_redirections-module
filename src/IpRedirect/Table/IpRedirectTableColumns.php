<?php namespace Fryiee\IpRedirectionsModule\IpRedirect\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class IpRedirectTableColumns
{
    public function handle(IpRedirectTableBuilder $builder)
    {
        $builder->setColumns([
            'name' => [
                'sort_column' => 'name'
            ],
            'enabled' => [
                'value' => 'entry.decorated.enabled.icon',
                'sort_column' => 'enabled'
            ],
            'exclusion_parameter',
            'exclusion_url' => [
                'value' => function ($entry) {
                    return '<a href="'.url('/?'.$entry->getAttribute('exclusion_parameter')).'" target="_blank">'.url('/?'.$entry->getAttribute('exclusion_parameter')).'</a>';
                },
                'sort_column' => 'exclusion_parameter'
            ],
            'destination_url' => [
                'value' => function ($entry) {
                    return '<a href="'.$entry->getAttribute('destination_url').'" target="_blank">'.$entry->getAttribute('destination_url').'</a>';
                },
                'sort_column' => 'destination_url'
            ]
        ]);
    }
}
