<?php namespace Fryiee\IpRedirectionsModule\IpRedirect;

use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class IpRedirectRepository extends EntryRepository implements IpRedirectRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var IpRedirectModel
     */
    protected $model;

    /**
     * Create a new IpRedirectRepository instance.
     *
     * @param IpRedirectModel $model
     */
    public function __construct(IpRedirectModel $model)
    {
        $this->model = $model;
    }
}
