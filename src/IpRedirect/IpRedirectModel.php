<?php namespace Fryiee\IpRedirectionsModule\IpRedirect;

use Fryiee\IpRedirectionsModule\IpRedirect\Contract\IpRedirectInterface;
use Anomaly\Streams\Platform\Model\IpRedirections\IpRedirectionsIpRedirectsEntryModel;

/**
 * Class IpRedirectModel
 * @package Fryiee\IpRedirectionsModule\IpRedirect
 */
class IpRedirectModel extends IpRedirectionsIpRedirectsEntryModel implements IpRedirectInterface
{
    /**
     * @return \Anomaly\Streams\Platform\Entry\EntryPresenter|mixed
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
}
