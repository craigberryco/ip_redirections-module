<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsAddInternalToRedirects extends Migration
{

    protected $delete = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $fields = [
        'internal' => 'anomaly.field_type.boolean',
    ];

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'ip_redirects',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'internal'
    ];
}
