<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsCreateBoundariesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'boundaries',
         'title_column' => 'id',
         'translatable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'northeast_latitude' => [
            'required' => true,
        ],
        'northeast_longitude' => [
            'required' => true
        ],
        'southwest_latitude' => [
            'required' => true
        ],
        'southwest_longitude' => [
            'required' => true
        ],
    ];

}
