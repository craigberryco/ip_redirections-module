<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsCreateIpRedirectionsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        // IP Redirects
        'name' => 'anomaly.field_type.text',
        'boundaries' => [
            'type' => 'anomaly.field_type.repeater',
            'config' => [
                'manage' => false,
                'related' => \Fryiee\IpRedirectionsModule\Boundary\BoundaryModel::class
            ]
        ],
        'destination_url' => 'anomaly.field_type.url',
        'enabled' => 'anomaly.field_type.boolean',
        'exclusion_parameter' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '_'
            ],
        ],


        // IP Coordinates
        'ipv4' => 'anomaly.field_type.text',
        'latitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],
        'longitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],

        // boundaries
        'northeast_latitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],
        'northeast_longitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],
        'southwest_latitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],
        'southwest_longitude' => [
            'type' => 'anomaly.field_type.decimal',
            'config' => [
                'decimals' => 8,
                'min' => -9999.99
            ]
        ],
    ];

}
