<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsAddLocationStringToBoundaries extends Migration
{

    protected $delete = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    protected $fields = [
        'location' => 'anomaly.field_type.text',
    ];

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'boundaries',
        'title_column' => 'id',
        'translatable' => false,
        'trashable' => false,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'location' => [
            'required' => true
        ]
    ];
}
