<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsCreateIpRedirectsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'ip_redirects',
         'title_column' => 'name',
         'translatable' => true,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'boundaries',
        'destination_url' => [
            'required' => true
        ],
        'enabled',
        'exclusion_parameter' => [
            'required' => true,
        ]
    ];

}
