<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FryieeModuleIpRedirectionsCreateIpCoordinatesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'ip_coordinates',
         'title_column' => 'ipv4',
         'translatable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'ipv4' => [
            'unique' => true,
            'required' => true,
        ],
        'latitude' => [
            'required' => true,
        ],
        'longitude' => [
            'required' => true,
        ],
    ];

}
